# Algorithms I Assignment 3: Collinear Points
Solution for assignment 3 of the Princeton University Algorithms I course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/collinear/specification.php).

This project showcases two different approaches to finding line segments of 4 or more points in a 2d plane. As per the assignment description, the solution in BruteCollinearPoints.java is intentionally bad to showcase the impracticality of quadratic or higher time complexities. FastCollinearPoints.java instead utilizes sorting with different comparators to solve the problem in n**2 log n time in the worst case.

Also includes a Point.java data structure to represent points, and a LineSegment.java data structure (included in starter code) for line segments.

## Results  

Correctness:  41/41 tests passed
Memory:       1/1 tests passed
Timing:       41/41 tests passed

<b>Aggregate score:</b> 100.00%


## How to use
Both BruteCollinearPoints and FastCollinearPoints have main methods that can be run with an appropriate .txt file as a command line argument to output a view of the detected line segments. The program accepts files consisting of an integer n on the first line, followed by n lines of 2 integers each representing the x and y coordinates of individual points.
