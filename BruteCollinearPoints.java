import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class BruteCollinearPoints {
    private int numSegments;

    private LineSegment[] segments;

    private Point[] copyArray(Point[] input) {
        int n = input.length;
        Point[] pointsArray = new Point[n];
        for (int i = 0; i < n; i++) {
            if (input[i] == null) throw new IllegalArgumentException();
            pointsArray[i] = input[i];
            for (int j = 0; j < n; j++) {
                if (input[j] == null || (i != j && input[i].compareTo(input[j]) == 0))
                    throw new IllegalArgumentException();
            }
        }
        return pointsArray;
    }

    private boolean duplicate(Point pi, Point pj, Point pk, Point pl) {
        return pi.compareTo(pj) == 0 || pi.compareTo(pk) == 0
                || pi.compareTo(pl) == 0;
    }


    public BruteCollinearPoints(Point[] points) {
        if (points == null) throw new IllegalArgumentException();
        Point[] pointsArray = copyArray(points);
        int N = pointsArray.length;
        numSegments = 0;
        segments = new LineSegment[N];
        for (int i = 0; i < N - 3; i++) {
            for (int j = i + 1; j < N - 2; j++) {
                for (int k = j + 1; k < N - 1; k++) {
                    for (int l = k + 1; l < N; l++) {
                        Point pi = pointsArray[i];
                        Point pj = pointsArray[j];
                        Point pk = pointsArray[k];
                        Point pl = pointsArray[l];
                        if (duplicate(pi, pj, pk, pl))
                            throw new IllegalArgumentException();
                        /*if (pi.slopeTo(pj) == Double.NEGATIVE_INFINITY ||
                                pi.slopeTo(pk) == Double.NEGATIVE_INFINITY ||
                                pi.slopeTo(pl) == Double.NEGATIVE_INFINITY
                        ) throw new IllegalArgumentException(); */
                        if (pi.slopeTo(pj) == pi.slopeTo(pk)
                                && pi.slopeTo(pj) == pi.slopeTo(pl)) {
                            Point[] colinnear = new Point[]
                                    {
                                            pi, pj, pk, pl
                                    };
                            Arrays.sort(colinnear);
                            segments[numSegments++] = new LineSegment(colinnear[0], colinnear[3]);
                        }
                    }
                }
            }
        }
    }


    public int numberOfSegments() {
        return numSegments;
    }

    public LineSegment[] segments() {
        LineSegment[] copy = new LineSegment[numSegments];
        for (int i = 0; i < numSegments; i++) {
            copy[i] = segments[i];
        }
        return copy;
    }

    /* Starter code provided by the course staff */
    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}

