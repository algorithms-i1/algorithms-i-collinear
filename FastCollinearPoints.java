import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;

public class FastCollinearPoints {
    private int numSegments;

    private ArrayList<LineSegment> segments;

    private boolean duplicate(Point pivot, Point q) {
        return pivot.slopeTo(q) == Double.NEGATIVE_INFINITY;
    }

    public FastCollinearPoints(Point[] points) {
        if (points == null) throw new IllegalArgumentException();
        Point[] pointsArray = copyArray(points);
        int N = pointsArray.length;
        numSegments = 0;
        segments = new ArrayList<LineSegment>();
        for (int i = 0; i < N; i++) {
            Arrays.sort(pointsArray);
            Point pivot = pointsArray[i];
            Arrays.sort(pointsArray, pivot.slopeOrder());
            if (N > 1 && duplicate(pivot, pointsArray[1]))
                throw new IllegalArgumentException();
            for (int endZone = 2; endZone < N; endZone++) {
                int startZone = endZone - 1;
                double slopeToStartZone = pivot.slopeTo(pointsArray[startZone]);
                while (endZone < N) {
                    if (slopeToStartZone == pivot.slopeTo(pointsArray[endZone]))
                        endZone++;
                    else break;
                }
                if (validLineSegment(endZone, startZone, pivot, pointsArray[startZone])) {
                    numSegments++;
                    segments.add(new LineSegment(pivot,
                                                 pointsArray[endZone
                                                         - 1]));
                }
            }
        }
    }

    private boolean validLineSegment(int end, int start, Point pivot, Point q) {
        return end - start >= 3 && pivot.compareTo(q)
                < 0;
    }

    private Point[] copyArray(Point[] input) {
        Point[] pointsArray = new Point[input.length];
        for (int i = 0; i < input.length; i++) {
            if (input[i] == null) throw new IllegalArgumentException();
            pointsArray[i] = input[i];
        }
        return pointsArray;
    }

    public int numberOfSegments() {
        return numSegments;
    }

    public LineSegment[] segments() {
        LineSegment[] asArray = new LineSegment[numSegments];
        for (int i = 0; i < numSegments; i++) {
            asArray[i] = segments.get(i);
        }
        return asArray;
    }

    /* Starter code provided by the course staff */
    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
